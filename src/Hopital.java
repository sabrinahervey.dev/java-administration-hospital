// Hopital.java (point d'entrée du programme) on teste ici les instances des autres classes 
//, verifie que tout fonctionne 
public class Hopital {
    public static void main(String[] args)  {
        //on crée les instances
        Patient patient = new Patient("Elsa", 23, "02454555455", null);
     
        //patient.addIllness(illness);
        Medication medication = new Medication("Paracétamol", "500mg");
        System.out.println(medication.getInfo());

        //doctor et nurse
        Doctor doctor = new Doctor("Grey", 52, "321251254-2235", "DG5621","surgeon");
        Nurse nurse = new Nurse("Paulette", 35, "7894562322", "4d");

        //illness
        Illness illness = new Illness("Covid");
        
       //test méthode
        System.out.println(illness.getInfo());
        illness.addMedication(medication);
        //affiche les instances
        System.out.println("Patient name: " + patient.getName() + " " + "numéro secu: " + patient.getSocialSecurityNumber()) ;

       doctor.getRole();
       nurse.getRole();

       doctor.careForPatient(patient);
       nurse.careForPatient(patient);

       doctor.recordPatientVisit("Elsa : illness = covid");



        
    }
}
