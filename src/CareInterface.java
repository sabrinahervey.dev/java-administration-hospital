interface CareInterface {
    void careForPatient(Patient patient);

    //void recordPatientVisit(String notes); 
    default void recordPatientVisit(String notes) {
        System.out.println(notes);
    }

    
}
