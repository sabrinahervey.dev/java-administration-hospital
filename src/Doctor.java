public class Doctor extends MedicalStaff {

    String specialty;

    public Doctor(String name, int age, String socialSecurityNumber, String employeeId, String specialty) {
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }
    
    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Doctor " + getName() + " cares for " + patient.getName());
    }
    
    @Override
    public void getRole() {
        System.out.println("Doctor");
    }
}
