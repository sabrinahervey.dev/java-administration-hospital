public class Medication {
   private String name;
   private String dosage;
   
   public Medication(String name, String dosage) {
    this.name = name;
    this.dosage = dosage;
   }
   

   public String getName() {
      return this.name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getDosage() {
      return this.dosage;
   }

   public void setDosage(String dosage) {
      this.dosage = dosage;
   }
   
   
   public String getInfo() {
    return "Médication: " + this.name + ", dosage: " + this.dosage;
   }
}